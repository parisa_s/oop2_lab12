package downloader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;

/** 
 * Mostly static utilities for URL operations to get info about a URL.
 */
public class URLUtils {
	
	/** Get a URL for a string name without throwing MalformedURLException. */
	public static URL getURL( String name ) {
		if ( name == null ) return null;
		name = name.trim();
		try {
			URL url = new URL( name );
			return url;
		} catch ( MalformedURLException e ) { 
			return null;
		}
	}
	
	/** Query a URL and return a long string of readable info. */
	public static String queryURL( URL url ) {
		StringBuffer sb = new StringBuffer();
		try {
			URLConnection connection = url.openConnection();
			sb.append( String.format("%20.20s: %s\n", "Hostname", url.getHost() ));
			sb.append( String.format("%20.20s: %s\n", "Filename", url.getPath() ));
			sb.append( String.format("%20.20s: %s\n", "Content-Length", connection.getContentLength() ));
			sb.append( String.format("%20.20s: %s\n", "Content-Type", connection.getContentType() ));
			sb.append( String.format("%20.20s: %s\n", "Content-Encoding", connection.getContentEncoding() ));
			sb.append( String.format("%20.20s: %Tc\n","Last Modified", connection.getLastModified() ));
			sb.append( String.format("%20.20s: %d\n", "Read Timeout", connection.getReadTimeout() ));
			connection = null;
		} catch (java.io.IOException e){
			sb.append( String.format("\nQueryURL caused an IOException:\n%s\n",e.toString() ));
		}
		return sb.toString();
	}
	
	/** Get the size in bytes of the resource at the specified url. */
	public static int getURLSize( URL url ) {
		if ( url == null ) return 0;
		try {
			URLConnection connection = url.openConnection();
			return connection.getContentLength();
		} catch (java.io.IOException e){
			System.out.println("\ngetURLSize caused an IOException: "+e.toString() );
			return 0;
		}
	}
	

	/** Get filename portion of URL.  Result may be an empty string. */
	public static String getURLFilename( URL url ) {		
		String filename = url.getPath();
		int k = filename.lastIndexOf("/");
		if (k == filename.length()-1) return "";
		if (k >= 0) filename = filename.substring(k+1);
		return filename;
	}
	
	/** Test the methods on console. */
	public static void main(String[] args) {
		Scanner console = new Scanner(System.in);
		System.out.print("Enter URL: ");
		String url = console.nextLine().trim();
		String info = queryURL(getURL(url));
		System.out.println( info );
	}
}
