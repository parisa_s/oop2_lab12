package downloader;

import javax.swing.SwingUtilities;

/**
 * Launch the URL downloader application.
 */
public class DownloadApp {

	/**
	 * Main method to start the user interface.
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater( new Runnable(){
			public void run(){
				DownloaderUI ui = new DownloaderUI();
				ui.run();
			}
		});
		
	}
}
